<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

ini_set("display_errors", 0);
/**
 * Database credentials.
 */
if ( strpos( $_SERVER['SERVER_NAME'], 'local' ) ) {

    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

    define( 'DB_NAME', 'csfabs-main-website' );
    define( 'DB_USER', 'root' );
    define( 'DB_PASSWORD', 'chinny2003' );
    define( 'DB_HOST', 'localhost' );

    /**
     * Custom constants used to build the local development URL.
     */
    define( 'WP_SITEURL', 'http://csfabs-website.local/');
    define( 'WP_HOME', 'http://csfabs-website.local/' );

    /**
     * Custom constants used rewrite the local development uploads with those on production.
     * Change the RR_DEV_REWRITE_UPLOADS constant to true to use this feature, ensuring that the RR_DEV_PRODUCTION_URL contsnat is also correct.
     */
    define( 'RR_DEV_REWRITE_UPLOADS', true);
    define( 'RR_DEV_PRODUCTION_URL', 'http://csfabs.co.uk/' );
    define( 'RR_DEV_LOCAL_UPLOADS', WP_SITEURL . 'wp-content/uploads' );
    define( 'RR_DEV_PRODUCTION_UPLOADS', RR_DEV_PRODUCTION_URL . 'wp-content/uploads' );

    if (RR_DEV_REWRITE_UPLOADS === true) {
        ob_start( function( $pageContent ) {
            return str_replace( array( RR_DEV_LOCAL_UPLOADS ), RR_DEV_PRODUCTION_UPLOADS, $pageContent );
        } );
	}


} else if ( strpos( $_SERVER['REQUEST_URI'], 'staging' ) === 1 ) {

    /**
     * Custom constants used to build the local development URL.
     */
    define( 'WP_SITEURL', 'https://csfabs.co.uk/staging/');
    define( 'WP_HOME', 'https://csfabs.co.uk/staging/' );

    define('DB_NAME', 'csfabs_stag01');
    define('DB_USER', 'csfabs_stag01');
    define('DB_PASSWORD', 'kLe3d0!j4at4i8');
    define('DB_HOST', 'localhost');

    /**
     * Custom constants used rewrite the local development uploads with those on production.
     * Change the RR_DEV_REWRITE_UPLOADS constant to true to use this feature, ensuring that the RR_DEV_PRODUCTION_URL contsnat is also correct.
     */
    define( 'RR_DEV_REWRITE_UPLOADS', true);
    define( 'RR_DEV_PRODUCTION_URL', 'http://cafabs.org.uk/' );
    define( 'RR_DEV_LOCAL_UPLOADS', WP_SITEURL . 'wp-content/uploads' );
    define( 'RR_DEV_PRODUCTION_UPLOADS', RR_DEV_PRODUCTION_URL . 'wp-content/uploads' );

    if (RR_DEV_REWRITE_UPLOADS === true) {
        ob_start( function( $pageContent ) {
            return str_replace( array( RR_DEV_LOCAL_UPLOADS ), RR_DEV_PRODUCTION_UPLOADS, $pageContent );
        } );
	}	

} else {
    define('DB_NAME', 'cl55-wplivedb');	
	define('DB_USER', 'cl55-wplivedb');
	define('DB_PASSWORD', 'RsrR3-sJ9');
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define( 'WP_AUTO_UPDATE_CORE', false );
define( 'WP_DEBUG', false );
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');







