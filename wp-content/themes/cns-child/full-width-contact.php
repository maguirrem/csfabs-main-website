<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Full Content Template
 *
Template Name:  Full Width Contact (no sidebar)
 *
 * @file           full-width-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content-full" class="grid col-940">

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php responsive_entry_top(); ?>

				<?php //get_template_part( 'post-meta', get_post_type() ); ?>

				<div class="post-entry">
                
                     
					<?php
					
						$bannerImage = get_field('slider_shortcode');		
						if(!empty($bannerImage))
						{
						?>
                        <div class="bannerPage">
                        	
                            <?php
							$bannerCaption = get_field('slider_shortcode');
							if(!empty($bannerCaption))
							{
								?>
                                <div class="caption">
								  	<?php print $bannerCaption; ?>
                                </div>
                                <?php
								
							}
							?>
                        </div>
                        <?php	
							
						}								
					
					?>
                   <div class="maxWidth contactPage">
                    <div>
                      <?php get_template_part( 'loop-header', get_post_type() ); ?>
                      <div class="grid col-620">
							
                            <?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
                            
                            
                      </div>
                      
                      <div class="grid col-300 fit contact300">    
                            <?php
                            
                            
                            // check if the flexible content field has rows of data
                            if( have_rows('content_contact') ):
                            
                                 // loop through the rows of data
                                while ( have_rows('content_contact') ) :                                 
                                    // get the number of columns						
                                    //print_r(get_sub_field_object('content'));
                                    the_row();	   
                                    
                                    if( get_row_layout() == 'contact_info' ): 
                                       ?>
                                            <div class="clearfix col-940 inline_banner_2_columns">
                                                
                                                    <?php the_sub_field('content_box')?>
                                                
                                            </div>
                                       <?php
                                      
                                    endif;
                            
                                endwhile;
                            
                            else :
                            
                                // no layouts found
                            
                            endif;
            
                            
                            ?>
                         </div>
                      </div>
                    </div>
                    
                    
                    
					<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
				</div>
				<!-- end of .post-entry -->

				<?php get_template_part( 'post-data', get_post_type() ); ?>

				<?php responsive_entry_bottom(); ?>
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

			<?php responsive_comments_before(); ?>
			<?php comments_template( '', true ); ?>
			<?php responsive_comments_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav', get_post_type() );

	else :

		get_template_part( 'loop-no-posts', get_post_type() );

	endif;
	?>

</div><!-- end of #content-full -->

<?php get_footer(); ?>
