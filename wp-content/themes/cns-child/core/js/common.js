/* Thanks to CSS Tricks for pointing out this bit of jQuery
 http://css-tricks.com/equal-height-blocks-in-rows/
 It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    jQuery(container).each(function() {

        $el = jQuery(this);
        jQuery($el).height('auto')
        topPostion = $el.offset().top;

        //console.log(topPostion, this);

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
            //rowDivs[currentDiv].find()
        }
    });
}

oldequalheight = function(container){

    var currentTallest = 0, $el;
    jQuery(container).each(function() {
        $el = jQuery(this);
        $el.height('auto');
        //console.log(currentTallest, $el.height(), this);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);

    });
    jQuery(container).height(currentTallest);
}



function setHeightTo(divHeightRef2Thirds, divsToSet, lessValue)
{
	var theHeight2Thirds = jQuery(divHeightRef2Thirds).height();
	jQuery(divsToSet).height(theHeight2Thirds-lessValue);

	//console.log(theHeight2Thirds + ' ' + divsToSet);
}

function setHeightToHighest(divHeightRef2Thirds, divsToSet, lessValue)
{
	var theHeight2Thirds = jQuery(divHeightRef2Thirds).height();
	
	
	var theHeightToSet = jQuery(divsToSet).height();
	//console.log(theHeight2Thirds + ' ' + theHeightToSet);
	if(theHeight2Thirds >=theHeightToSet)
	{		
		jQuery(divsToSet).height(theHeight2Thirds-lessValue);
		//console.log('@ca');
		//jQuery(divHeightRef2Thirds).height(theHeight2Thirds);
	}
	else
	{
	   // jQuery(divsToSet).height(theHeightToSet);
		jQuery(divHeightRef2Thirds).height(theHeightToSet-lessValue);
		//console.log('@cu');
	}

	//console.log(theHeight2Thirds + ' ' + divsToSet);
}


jQuery(window).load(function() {

	equalheight('.widgetRow .grid');
	
	setHeightToHighest('#post-149 .cover-0 .gridGeneric .bgWidgetImage', '#post-149 .cover-0 .gridGeneric .bridgeHeight', 40);
	setHeightToHighest('#post-149 .cover-1 .gridGeneric .bgWidgetImage', '#post-149 .cover-1 .gridGeneric .bridgeHeight', 40);
	
	//setHeightToHighest('#post-151 .cover-0 .col-460 .bgWidgetImage', '#post-151 .cover-0 .col-460 .bridgeHeight', 40);
	
	setHeightToHighest('.cover-0 .gridGeneric .bgWidgetImage', '.cover-0 .gridGeneric .bridgeHeight', 0);
	setHeightToHighest('.cover-1 .gridGeneric .bgWidgetImage', '.cover-1 .gridGeneric .bridgeHeight', 0);
	
	
	
	equalheight('.home .widgetRow .widgetWrap');
	equalheight('.widgetRow .widgetContent');
	equalheight('.widgetRow .bgImageW');
	//equalheight('.genericBgWidgets .grid');
	
	if ( window.innerWidth < 768 ) {
		//jQuery('.fancybox').click(false);
		//jQuery('.fancybox').off("click");
	}
	
});





jQuery(window).resize(function(){
	equalheight('.widgetRow .grid');	
    //setHeightToHighest('.widgetRow .gridGeneric .bgWidgetImage', '.gridGeneric .bridgeHeight', 0);
	
	setHeightToHighest('#post-149 .cover-0 .gridGeneric .bgWidgetImage', '#post-149 .cover-0 .gridGeneric .bridgeHeight', 40);
	setHeightToHighest('#post-149 .cover-1 .gridGeneric .bgWidgetImage', '#post-149 .cover-1 .gridGeneric .bridgeHeight', 40);	
	
	setHeightToHighest('.cover-0 .gridGeneric .bgWidgetImage', '.cover-0 .gridGeneric .bridgeHeight', 0);
	setHeightToHighest('.cover-1 .gridGeneric .bgWidgetImage', '.cover-1 .gridGeneric .bridgeHeight', 0);
	
	equalheight('.home .widgetRow .widgetWrap');
	equalheight('.widgetRow .widgetContent');	
	equalheight('.widgetRow .bgImageW');
	//equalheight('.genericBgWidgets .grid');
	//setHeightTo('.widgetRow .grid', '.widgetRow .widgetWrap', 40);	
	
	
	
});
