<?php
/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

		<?php wp_head(); ?>
        
        <script src="<?=home_url()?>/wp-content/themes/cns-child/core/js/common.js"></script>
        
        
        
         <?php
        if(is_page_template( 'full-width-contact.php'))
		{ ?>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAg_CGjnfIgNlwAPH5EyVTt8itdA7PrT1k&sensor=false"></script>  
        
         <script language="javascript" type="text/javascript">
          var map;                   
        
          function initialize() 
          { // function initialise the position on the map
              
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(53.401037, -1.486229);
                var myOptions = {
                  zoom: 15,
                  center: latlng,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                
				
				
				//custom icon
				//alert('Hola');
				// Define Marker properties
				var image = new google.maps.MarkerImage('http://csfab.cdpcreative.com/wp-content/themes/cns-child/core/images/cnspin.png',
					// This marker is 129 pixels wide by 42 pixels tall.
					new google.maps.Size(129, 55),
					// The origin for this image is 0,0.
					new google.maps.Point(0,0),
					// The anchor for this image is the base of the flagpole at 18,42.
					new google.maps.Point(18, 42)
				);
			 
				// Add Marker
				var marker1 = new google.maps.Marker({
					position: new google.maps.LatLng(53.401037, -1.486229),
					map: map,
					icon: image // This path is the custom pin to be shown. Remove this line and the proceeding comma to use default pin
				});
				
								
				
				 var infoWindow = new google.maps.InfoWindow({
                     content: '<strong>C&S Fabrications LTD</strong>'
                 });

                 google.maps.event.addListener(marker1, 'click', function () {
                     infoWindow.open(map, marker1);
                 });
	             infoWindow.open(map, marker1);
			
				
          }
        
			
		</script>
        <?php
		}
		?>  
        <!-- End -->        
        
        
        
        
	</head>

<body <?php body_class(); ?> <?php if(is_page_template( 'full-width-contact.php')){ ?> onLoad="initialize()" <?php } ?>>

<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">

<?php responsive_header(); // before header hook ?>
	<div class="skip-container cf">
		<a class="skip-link screen-reader-text focusable" href="#content"><?php _e( '&darr; Skip to Main Content', 'responsive' ); ?></a>
	</div><!-- .skip-container -->
	<div id="header" role="banner">
       <div id="headerIn">
       
        <div class="grid col-620 fit">
            <div class="right">
                 <span class="hide650 findMore">FIND OUT MORE</span>
                 <span class="findMore thePhone">T: 0114 234 7567</span>
                 <span class="hide650 findMore"><a href="mailto:info@csfabs.co.uk" target="_blank">info@csfabs.co.uk</a></span>
            </div>
         </div>
       
        <div class="grid col-300">
		<?php responsive_header_top(); // before header content hook ?>

		<?php if ( has_nav_menu( 'top-menu', 'responsive' ) ) {
			wp_nav_menu( array(
				'container'      => '',
				'fallback_cb'    => false,
				'menu_class'     => 'top-menu',
				'theme_location' => 'top-menu'
			) );
		} ?>

		<?php responsive_in_header(); // header hook ?>

		<?php if ( get_header_image() != '' ) : ?>

			<div id="logo">
				<a href="<?php echo esc_url(home_url( '/' )); ?>"><img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>"/></a>
			</div><!-- end of #logo -->

		<?php endif; // header image was removed ?>

		<?php if ( !get_header_image() ) : ?>

			<div id="logo">
				<span class="site-name"><a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
				<span class="site-description"><?php bloginfo( 'description' ); ?></span>
			</div><!-- end of #logo -->

		<?php endif; // header image was removed (again) ?>
         </div>
         
        </div><!-- In Head-->
        <div class="fullWidth bgBlue" id="theMainNav">
          <div class="maxWidth">
		<?php get_sidebar( 'top' ); ?>
		<?php wp_nav_menu( array(
			'container'       => 'div',
			'container_class' => 'main-nav',
			'fallback_cb'     => 'responsive_fallback_menu',
			'theme_location'  => 'header-menu'
		) ); ?>

		<?php if ( has_nav_menu( 'sub-header-menu', 'responsive' ) ) {
			wp_nav_menu( array(
				'container'      => '',
				'menu_class'     => 'sub-header-menu',
				'theme_location' => 'sub-header-menu'
			) );
		} ?>

		<?php responsive_header_bottom(); // after header content hook ?>
           </div>
         </div>


	</div><!-- end of #header -->
    
    
<?php responsive_header_end(); // after header container hook ?>

<?php responsive_wrapper(); // before wrapper container hook ?>
	<div id="wrapper" class="clearfix">
<?php responsive_wrapper_top(); // before wrapper content hook ?>
<?php responsive_in_wrapper(); // wrapper hook ?>
