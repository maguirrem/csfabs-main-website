<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Full Content Template
 *
Template Name:  Full Width About (no sidebar)
 *
 * @file           full-width-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content-full" class="grid col-940">

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php responsive_entry_top(); ?>

				<?php //get_template_part( 'post-meta', get_post_type() ); ?>

				<div class="post-entry">
                
                     
					<?php
					
						$bannerImage = get_field('slider_shortcode');		
						if(!empty($bannerImage))
						{
						?>
                        <div class="bannerPage">
                        	
                            <?php
							$bannerCaption = get_field('slider_shortcode');
							if(!empty($bannerCaption))
							{
								?>
                                <div class="caption">
								  	<?php print $bannerCaption; ?>
                                </div>
                                <?php
								
							}
							?>
                        </div>
                        <?php	
							
						}								
					
					?>
                   <div class="maxWidthSmall">
                    <?php get_template_part( 'loop-header', get_post_type() ); ?>
					<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
                    
                    
                   
					<?php
					
					
					// check if the flexible content field has rows of data
					if( have_rows('content_about') ):
					
						 // loop through the rows of data
						while ( have_rows('content_about') ) : 
						
						    // get the number of columns						
							//print_r(get_sub_field_object('content'));
							the_row();						
   
							
							if( get_row_layout() == 'content_block' ): 
					           ?>
									<div class="clearfix col-940 inline_banner_2_columns <?=the_sub_field('background_colour')?>">
                                        
                                            <?php the_sub_field('content_box')?>
                                        
                                    </div>
					           <?php
							
							
							elseif( get_row_layout() == 'two_widgets' ): 
					           
							   $theRow = get_row();
							   
							   $nWidgets =  count($theRow["field_58b802048ff9d"]);
							   $countWidget = 1;
							   $fitClass = "";
							   ?>
									<div class="widgetRow aboutBgWidgets" >
                                         
                                         
                                         <?php 
										 while( have_rows('widget_about') ): the_row(); 
										// if(($countWidget == 2 && $nWidgets ==2) || ($countWidget == 3 && $nWidgets ==3)) $fitClass = " fit";
										 
										 $theBG = false;
										 $widget_image = get_sub_field('widget_image');
										 if(!empty($widget_image))
										 {
											 $theBG = 'style="background: url('.get_sub_field('widget_image').') no-repeat center center"';
											 ?>
                                             <style>
											 	.bgWidgetImage
												{
													   background-size: cover!important;
													  -webkit-background-size: cover;
													  -moz-background-size: cover;
													  -o-background-size: cover;
													 
												}
											 </style>
                                             <?php
											 
										 }										 
										 ?>
 											<div class="grid col-460  <?php if($countWidget%2==0) echo "fit" ?>" >
                                                <div class="widgetWrap  frontW bgWidgetImage" <?php if(!empty($theBG)) echo $theBG?>>                                           
                                                   <h2><?=get_sub_field('widget_title')?></h2>                                                   <?php
												   $widget_content = get_sub_field('widget_content');
												   if(!empty($widget_content))
													 {
												   ?>                                                
													                                             
														 <?=get_sub_field('widget_content')?>
													 
													<?php
													 }
													?>
                                                   <?php
												   $widget_link = get_sub_field('widget_link');
												   if(!empty($widget_link))
												   {
												   ?>                                                     
                                                    <a href="<?=get_sub_field('widget_link')?>">Find Out More</a> 
                                                    <?php
												   }
													?>                                         
                                                </div>
                                                
                                            </div>  
                                            
                                          <?php 
										  $countWidget++;
										  endwhile; ?>                                         
                                         
                                         
                                    </div>
					           <?php
							   
							elseif( get_row_layout() == 'two_images' ): 
					           
							   $theRow = get_row();
							   
							   $nWidgets =  count($theRow["field_58b802048ff9d"]);
							   $countWidget = 1;
							   $fitClass = "";
							   ?>
									<div class="widgetRow aboutWidgetsImage " >
                                         
                                         
                                         <?php 
										 while( have_rows('content_image') ): the_row(); 
										// if(($countWidget == 2 && $nWidgets ==2) || ($countWidget == 3 && $nWidgets ==3)) $fitClass = " fit";
										 
										 $theBG = false;
																 
										 ?>
 											<div class="noBG grid col-460  <?php if($countWidget%2==0) echo "fit" ?>" >
                                                <div class="widgetWrap  frontW bgWidgetImage " <?php if(!empty($theBG)) echo $theBG?>>                                           
                                                   <?php
												   $the_image = get_sub_field('the_image');
												   if(!empty($the_image))
												   {
													?>                                           
														 <img src="<?=get_sub_field('the_image')?>" alt="<?=get_sub_field('image_alt_tag')?>"/> 
													<?php
												   }
													?>
                                                                                     
                                                </div>
                                                
                                            </div>  
                                            
                                          <?php 
										  $countWidget++;
										  endwhile; ?>                                         
                                         
                                         
                                    </div>
					           <?php
							   
							     
							
							  
							endif;
					
						endwhile;
					
					else :
					
						// no layouts found
					
					endif;

						
				
					
					
					?>
                    
                    </div>
                    
                    
                    
                    
					<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
				</div>
				<!-- end of .post-entry -->

				<?php get_template_part( 'post-data', get_post_type() ); ?>

				<?php responsive_entry_bottom(); ?>
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

			<?php responsive_comments_before(); ?>
			<?php comments_template( '', true ); ?>
			<?php responsive_comments_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav', get_post_type() );

	else :

		get_template_part( 'loop-no-posts', get_post_type() );

	endif;
	?>

</div><!-- end of #content-full -->

<?php get_footer(); ?>
