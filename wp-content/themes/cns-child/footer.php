<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/*
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
?>
<?php responsive_wrapper_bottom(); // after wrapper content hook ?>
</div><!-- end of #wrapper -->
<?php responsive_wrapper_end(); // after wrapper hook ?>
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>

<div id="footer" class="clearfix" role="contentinfo">
 <div class="maxWidth">
	<?php responsive_footer_top(); ?>

	<div id="footer-wrapper">

		<?php get_sidebar( 'footer' ); ?>

		<div class="grid col-940">

			<div class="grid col-540">
				<a><img src="<?=home_url()?>/wp-content/themes/cns-child/core/images/logo-footer.jpg"/></a>
			</div><!-- end of col-540 -->

			<div class="grid col-380 fit">
            
                  <div class="grid col-460" id="cnsAddress">
                  
                      C&S Fabrications Ltd<br />
                      Club Mill Road<br />
                      Hillsborough<br />
                      Sheffield S6 2FH<br />
                      <br />
                      <strong>0114 234 7567</strong>
                      <br />
                      info@csfabs.co.uk
                  
                  </div>
                  
                  <div class="grid col-460 fit">
                      <div class="right">
                        <?php echo responsive_get_social_icons() ?>
                        
                        <?php if ( has_nav_menu( 'footer-menu', 'responsive' ) ) {
                            wp_nav_menu( array(
                                'container'      => '',
                                'fallback_cb'    => false,
                                'menu_class'     => 'footer-menu',
                                'theme_location' => 'footer-menu'
                            ) );
                        } ?>                
                      </div> 
                  </div> 
			</div><!-- end of col-380 fit -->

		</div><!-- end of col-940 -->
		<?php get_sidebar( 'colophon' ); ?>
        <div class="grid col-940" id="dividerCopy">
            <div class="grid col-300 copyright">
                <?php esc_attr_e( '&copy;', 'responsive' ); ?> <?php echo date( 'Y' ); ?><a id="copyright_link" href="<?php echo esc_url( home_url( '/' ) ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    <?php bloginfo( 'name' ); ?>
                </a>
            </div><!-- end of .copyright -->
    
            <div class="grid col-300 scroll-top"><a href="#scroll-top" title="<?php esc_attr_e( 'scroll to top', 'responsive' ); ?>"><?php _e( '&uarr;', 'responsive' ); ?></a></div>
    
            <div class="grid col-300 fit pptc" >
               <div class="inPPTC">
                <a href="<?=home_url( '/' )?>about">About</a>  /  <a href="<?=home_url( '/' )?>contact-us" >Contact</a>
               </div>
            </div><!-- end .powered -->
        </div>
	</div><!-- end #footer-wrapper -->

	<?php responsive_footer_bottom(); ?>
  </div>
</div><!-- end #footer -->
<?php responsive_footer_after(); ?>

<?php wp_footer(); ?>
</body>
</html>
