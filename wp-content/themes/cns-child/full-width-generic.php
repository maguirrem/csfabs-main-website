<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Full Content Template
 *
Template Name:  Full Width Internal (no sidebar)
 *
 * @file           full-width-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content-full" class="grid col-940">

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php responsive_entry_top(); ?>

				<?php //get_template_part( 'post-meta', get_post_type() ); ?>

				<div class="post-entry">
                
                     
					<?php
					
						$bannerImage = get_field('slider_shortcode');		
						if(!empty($bannerImage))
						{
						?>
                        <div class="bannerPage">
                        	
                            <?php
							$bannerCaption = get_field('slider_shortcode');
							if(!empty($bannerCaption))
							{
								?>
                                <div class="caption">
								  	<?php print $bannerCaption; ?>
                                </div>
                                <?php
								
							}
							?>
                        </div>
                        <?php	
							
						}								
					
					?>
                   <div class="maxWidth">
                    <?php get_template_part( 'loop-header', get_post_type() ); ?>
					<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
                    
                    
                   
					<?php
					
					
					// check if the flexible content field has rows of data
					if( have_rows('content_internal') ):
					
						$countWidgetMain=0;
						// loop through the rows of data
						while ( have_rows('content_internal') ) : 
						
						    // get the number of columns						
							//print_r(get_sub_field_object('content'));
							the_row();						
   							
							
							if( get_row_layout() == 'content_block' ): 
					           ?>
									<div class="clearfix col-940 inline_banner_2_columns <?=the_sub_field('background_colour')?>">
                                        
                                            <?php the_sub_field('content_box')?>
                                        
                                    </div>
					           <?php
							
							
							elseif( get_row_layout() == 'two_widgets' ): 
					           
							   $theRow = get_row();
							   
							   $nWidgets =  count($theRow["field_58b802048ff9d"]);
							   $countWidget = 1;
							   $fitClass = "";
							   ?>
                               
                               <div class="<?="cover-".$countWidgetMain?>">
									<div class="widgetRow genericBgWidgets">                                         
                                         
                                         <?php 
										 while( have_rows('widget_internal') ): the_row(); 
										// if(($countWidget == 2 && $nWidgets ==2) || ($countWidget == 3 && $nWidgets ==3)) $fitClass = " fit";
										 
										 $theBG = false;
										 $widget_image = get_sub_field('widget_image');
										 if(!empty($widget_image))
										 {
											 $theBG = 'style="background: url('.$widget_image.') no-repeat center center"';
											 ?>
                                             <style>
											 	.bgWidgetImage
												{
													   background-size: cover!important;
													  -webkit-background-size: cover;
													  -moz-background-size: cover;
													  -o-background-size: cover;
													 
												}
											 </style>
                                             <?php											 
										 }	
										 
										 $soloImage = "bgImageW";
										 $bridgeHeight = "";
										 $squareImg = "";
										 if(get_sub_field('widget_type') == "imgOnly")
										 {
											   $soloImage = "soloImage";	
											   $bridgeHeight = "bridgeHeight";
										 }
										 $square_image_only = get_sub_field('square_image_only');
										 if(!empty($square_image_only)) $squareImg = "squareImage";
										 
										 if(get_sub_field('widget_text_colour') == "txtWhite")  $whiteText = "whiteText";	 									 										 ?>
 											<div class="<?=$whiteText?> <?=$soloImage?> gridGeneric col-460  <?php if($countWidget%2==0) echo "fit" ?>" >
                                                <div class="widgetWrap  <?=$bridgeHeight?> <?=$squareImg?> frontW bgWidgetImage" <?php if(!empty($theBG)) echo $theBG?> style="clear:both">                                                 <?php
												 $widget_title = get_sub_field('widget_title');
                                                 if(!empty($widget_title) && $soloImage != "soloImage")
										         {
												  ?>                                                
                                                   <h2><?=$widget_title?></h2>
                                                 <?php
												 }
												 ?>  
                                                   
                                                    <?php
													$widget_content = get_sub_field('widget_content');
												    if(!empty($widget_content))
												    {
													?> 	                                             
														 <?=$widget_content?>										     
													<?php
													}
												   ?>
                                                   <?php
												   $widget_image = get_sub_field('widget_image');
                                                   if(!empty($widget_image))
										 		   {
													?>
                                                    
                                                      <div class="show480">
                                                         <img src="<?=$widget_image?>" />
                                                      </div>
                                                    
                                                    <?php														
												   }													
												   ?>
                                                                                                   
                                                   
                                                   <?php
												   $widget_link = get_sub_field('widget_link') ;
												   if(!empty($widget_link))
												   {
												   ?>                                                     
                                                    <a href="<?=$widget_link?>">Find Out More</a> 
                                                    <?php
												   }
													?>                                         
                                                </div>
                                                 <?php
                                                     if(!empty($widget_title) && $soloImage == "soloImage")
                                                     {
                                                      ?> 
                                                     
                                                      <div class="theCaption">                                               
                                                       <?=$widget_title?>
                                                      </div>
                                                     <?php
                                                     }
                                                     ?>  
                                                
                                                
                                            </div>  
                                            
                                          <?php 
										  $countWidget++;
										  endwhile; ?>                                         
                                         
                                         
                                    </div>
                                  </div>
					           <?php
							   
							   
							   
							   
							   
							   
							   
							   
							   
							   
							   elseif( get_row_layout() == 'gallery' ): 
					           
							   $theRow = get_row();
							   
							   $nWidgets =  count($theRow["field_58b802048ff9d"]);
							   $countWidget = 1;
							   $fitClass = "";
							   // print_r($theRow);
							   // echo the_sub_field("gallery_title");
							   $theRowID = "";
							   if(isset($theRow["field_58bff187fbf62"]))
							   		$theRowID = $theRow["field_58bff187fbf62"];
							   
							   
							   $gallery_image_background = get_sub_field('gallery_image_background');
							   ?>
									<div class="clear"></div>
                                    
                                    <?php
									 $theBGGal = false;
										
									 if(!empty($gallery_image_background))
									 {
										 $theBGGal = 'style="background: url('.$gallery_image_background.') no-repeat center center"';
									 }
									?>
                                    
                                    
                                    <div class="widgetRow bgGalleryRow " <?=$theBGGal?>>                                         
                                         <?php
										 $gallery_title = get_sub_field('gallery_title');
                                         if(!empty($gallery_title))
									     {
										 ?> 
                                            <div>
                                               <h2><?=$gallery_title?></h2>
                                            </div>
                                         
                                         <?php
                                         }
                                         ?>
                                         <div style="text-align: center;" class="<?php if($theRowID == 303) echo"mini-max-width"; else echo"normal-max-width"; ?>">
                                         <?php 
										 while( have_rows('the_gallery') ): the_row(); 
											//gallery_image_background										 
										 ?>
 											<div class="grid col-140 hole-gallery">
                                                <div class="widgetWrap  frontW" <?php if(!empty($theBG)) echo $theBG?> style="clear:both">                                                 
												<?php
                                                   $picture =  get_sub_field('picture');
                                                   if(!empty($picture))
										 		   {
													?> 
                                                         <a href="<?=$picture?>" class="fancybox"><img src="<?=$picture?>" alt="<?=get_sub_field('picture_alt_tag')?>" /></a>
                                                                                                      
                                                    <?php														
												   }													
												   ?>
                                                                                        
                                                </div>                                                 
                                                
                                            </div>  
                                            
                                          <?php 
										  $countWidget++;
										  endwhile; ?>                                         
                                        
                                        </div> 
                                    </div>
					           <?php
					
							   
							   
							   
							   
							  
							endif;
					    $countWidgetMain++;
						endwhile;
					   
					else :
					
						// no layouts found
					
					endif;

						
				
					
					
					?>
                    
                    </div>
                    
                    
                    
                    
					<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
				</div>
				<!-- end of .post-entry -->

				<?php get_template_part( 'post-data', get_post_type() ); ?>

				<?php responsive_entry_bottom(); ?>
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

			<?php responsive_comments_before(); ?>
			<?php comments_template( '', true ); ?>
			<?php responsive_comments_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav', get_post_type() );

	else :

		get_template_part( 'loop-no-posts', get_post_type() );

	endif;
	?>

</div><!-- end of #content-full -->

<?php get_footer(); ?>
